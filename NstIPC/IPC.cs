﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation.Collections;

namespace NstIPC
{
    class IPC
    {

        // Receives Response / Alert / Telemetry data from PortManager    
        // Routes data from PortManager to the suitable NstApplication
        public async static Task Routing(object Message)
        {
            try
            {
                // Received data from PortManager
                string inputMessage = Message.ToString();
                Debug.WriteLine("Data Received: " + inputMessage);

                // Routes data from Portmanager to the suitable NstApplication
                await sendToNstAppwithLaunchUri(inputMessage);

            }
            catch (AggregateException ex)
            {
                foreach (Exception exception in ex.InnerExceptions)
                {
                    Debug.WriteLine("NstIPC - Routing: ", exception);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NstIPC - Routing: ", ex.Message);
            }

        }
       

        private async static Task sendToNstAppwithLaunchUri(string message)
        {
            try
            {
                var options = new Windows.System.LauncherOptions();
                
                // NstApplication  package family name 
                options.TargetApplicationPackageFamilyName =   "fe1dfef0-4e97-48f6-85c1-4e13380c3b51_dryy15wfsn6zj";
               
                //options.DesiredRemainingView = Windows.UI.ViewManagement.ViewSizePreference.UseHalf;

                ValueSet sentData = new ValueSet();
                sentData.Add("Token", message);
                sentData.Add("Sender", "NstIPC");
                Uri uri = new Uri("nst.gateway.nstapplication:");
                bool success = await Windows.System.Launcher.LaunchUriAsync(uri, options, sentData);
                if (!success)
                {
                    Debug.WriteLine("NstApplication can not be launched");                   
                }
                sentData.Clear();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendToNstAppwithLaunchUri: Exception thrown: " + ex.Message);
            }
        }


        // A command received - Route it to Serial Interface (PortManager)
        public async static Task processNstCommandReceived(object in_object)
        {
            try
            {
                string message = in_object.ToString();
                Debug.WriteLine("--- Nst App: COMMAND RECEIVED: " + message);

                var options = new Windows.System.LauncherOptions();

                //Serial Interface Package Family name
                options.TargetApplicationPackageFamilyName = "ee24107e-f75c-448f-89a1-0f5db640cd19_1vb1x38y1cdjy";

                ValueSet sentData = new ValueSet();
                sentData.Add("Token", message);
                sentData.Add("Sender", "NstIPC");
                Uri uri = new Uri("nst.gateway.serialinterface:");
                bool success = await Windows.System.Launcher.LaunchUriAsync(uri, options, sentData);
                if (!success)
                {
                    Debug.WriteLine("Serial Interface can not be launched");              
                }
                sentData.Clear();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("processNstCommandReceived: Exception thrown: " + ex.Message);

            }
        }

    }
}
